# REPL (Real Eval Print Loop)
REPL is a simple interactive environment that takes single user inputs, executes them, and returns the result to the user. The REPL feature lets you inspect your dependency graph and call methods on your providers (and controllers) directly from your terminal.


````sh
$ nest start "--entryFile" "repl"
[excelon.yuk@localhost /d/Git/example/repl]# nest start "--entryFile" "repl"
[Nest] 88096  - 2023. 02. 22. 오후 2:20:27     LOG [NestFactory] Starting Nest application...
[Nest] 88096  - 2023. 02. 22. 오후 2:20:27     LOG [InstanceLoader] AppModule dependencies initialized
[Nest] 88096  - 2023. 02. 22. 오후 2:20:27     LOG REPL initialized
> help()
You can call .help on any function listed below (e.g.: help.help):

$ - Retrieves an instance of either injectable or controller, otherwise, throws exception.
debug - Print all registered modules as a list together with their controllers and providers.
If the argument is passed in, for example, "debug(MyModule)" then it will only print components of this specific module.
get - Retrieves an instance of either injectable or controller, otherwise, throws exception.
help - Display all available REPL native functions.
methods - Display all public methods available on a given provider or controller.
resolve - Resolves transient or request-scoped instance of either injectable or controller, otherwise, throws exception.
select - Allows navigating through the modules tree, for example, to pull out a specific instance from the selected module.
> debug()

AppModule:
 - controllers:
  ◻ AppController
 - providers:
  ◻ AppService

> methods(AppService)

Methods:
 ◻ getHello

> get(AppService).getHello()
'Hello World!'
> 
````
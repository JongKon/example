### body-parser
- request의 본문을 해석해주는 미들웨어 / request body 부분을 해석해서 req.body 속성으로 사용하게 해주는 미들웨어
- JSON,  URL-encoded, Raw, Text 형식의 본문을 해석할 수 있다
- multipart/form-data 같은 폼을 통해 전송된 데이터는 해석하지 못함

````javascript
app.use(bodyParser.raw());
app.use(bodyParser.text());
````

### body-parser 설치
$ npm i  body-parser

### 예제 관련 패키지 설치
$ npm i express

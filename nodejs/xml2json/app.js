// import xml2Json from 'xml2js';
const xml2Json = require( "xml2js");
const fs = require("fs").promises;

async function main(){

  //  console.log(await readXML('ddd'));
    await readXML(__dirname + '\\test.xml');

    async function readXML(xmlFileName) {
        let xmlData = await fs.readFile(xmlFileName,'utf-8');

        xml2Json.parseString(xmlData, function(err, result){
            if(err) {
                console.log(err);
            }
            else {   // json 출력
                console.log(result['Page']['Page_Content'][0]['Component']);

            }     
        })
    };
    
}

main();

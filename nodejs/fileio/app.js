// file io sync에 주의할 필요가 있음
// function을 만들고 asysnc await 활용
// ==> await는 최상위레벨에서는 사용이 안되므로, 최상위 function 을 aysnc 로 만들고 그 내부에서 await를 사용하도록 한다.

let fs = require("fs").promises;

async function main(){
    console.log(__dirname);

    // 1. file write
    let fileText = "난 지난 여름에 네가 한 짓을 알고있을 껄 ?\n"
    await fs.writeFile(__dirname + "/test.txt", fileText,
        async function(err){
            if(err)    console.log(err.message,'Creation Fail');
            else console.log('Creation Success');
        }
    );

    // file append
    let addText = '난 더붙여지는 인생이야 ?\n'
    
    await fs.appendFile(__dirname + "/test.txt", addText,
       async function(err){
            if(err)    console.log(err.message,'Appending Fail');
            else console.log('Appending Success');
        }
    );

    // file read
    let readData = await fs.readFile(__dirname + "/test.txt",'utf-8');
    console.log(readData.toString());
}
main();

//===================
    // // 1. file write
    // let fileText = "난 지난 여름에 네가 한 짓을 알고있을 껄 ?\n"
    // fs.writeFile(__dirname + "/test.txt", fileText,
    //     async function(err){
    
    //         if(err)    console.log(err.message,'Creation Fail');
    //         else console.log('Creation Success');
    //     }
    // );
    
    // // 1.1 file 읽기 1
    // console.log('file read by way 1')
    // console.log(fs.readFileSync(__dirname + "/test.txt").toString());
    // console.log('============================');
    
    
    // // 1.2  file 읽기 2
    // console.log('file read by way 2')
    // let fdata = fs.readFile(__dirname + "/test.txt", 'utf-8',
    //     async function(err, datas){
    //         if(err)    console.log(err.message,'Reading Fail');
    //         else {
    //             // console.log('Reading Success');
    //             console.log(datas)
    //         }
    //     }
    // );
    
    
    // // 2. add text to file
    // let addText = '난 더붙여지는 인생이야 ?'
    
    // fs.appendFile(__dirname + "/test.txt", addText,
    //    async function(err){
    
    //         if(err)    console.log(err.message,'Appending Fail');
    //         else console.log('Appending Success');
    //     }
    // );
    
    // // 2.1
    // console.log('file append by way 1')
    // console.log(fs.readFileSync(__dirname + "/test.txt").toString());
    // // 1.2  file 읽기 2
    // console.log('file append by way 2')
    // let fdata2 = fs.readFile(__dirname + "/test.txt", 'utf-8',
    //  function(err, datas){
    //         if(err)    console.log(err.message,'Reading Fail');
    //         else {
    //             // console.log('Reading Success');
    //             console.log(datas)
    //         }
    //     }
    // );
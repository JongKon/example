const ExcelJS =  require('exceljs');

const workbook = new ExcelJS.Workbook();

const worksheet = workbook.addWorksheet();
const filename = 'test_col';

        const rawData = [
            {header: "order_id", data: ['12345678', '12345679', '12345680']},
            {header: "store_id", data: ['storeA', 'storeB', 'storeC']},
            {header: "country_id", data: ['KR', 'KR', 'KR']},
            {header: "price", data: ['15000', '10000', '20000']}
        ]

        rawData.forEach((data, index) => {
            worksheet.getColumn(index + 1).values = [data.header,  ...data.data];
        });

        workbook.xlsx.writeFile(`./${filename}.xlsx`);
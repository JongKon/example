const ExcelJS =  require('exceljs');

const workbook = new ExcelJS.Workbook();

const worksheet = workbook.addWorksheet();
const filename = 'test_row';

// const headerData = [ 
//         { data: '순번'   },
//         { data: '헤더1'  },
//         { data: '헤더2'  },
//         { data: '헤더3'  },
    
//     ];

//     const headerData = [ { data : ['순번' ,'헤더1', '헤더2', '헤더3'] }]
    const headerData = ['순번' ,'헤더1', '헤더2', '헤더3'] ;


        const rawData = [
            { data: ['12345678', 'storeA', 'KR','15000', null]},
            { data: ['12345679', 'storeB', 'UK',16000, true]},
            { data: ['12345680', 'storeC', 'KR','20000', false]},
            { data: ['12345611', 'storeD', 'JP',10000]}
            
        ]

        //  Header
        worksheet.getRow(1).values = [...headerData];
     
        // rawData 
        rawData.forEach((data, index) => {
            worksheet.getRow(index + 2).values = [index, ...data.data];
        });

        workbook.xlsx.writeFile(`./${filename}.xlsx`);
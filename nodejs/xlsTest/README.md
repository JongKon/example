# 혹시나 Nodejs를 사용하여 엑셀 파일을 생성하다면

## 개요
1. 엑셀 파일을 생성하고 data를 쓴다
2. 헤더 및 raw data를 쓰는 방법에 따라
    - colum 기준을 write하는 것(app_col.js 참조)
    - row 기준으로 write하는 것(app_row.js 참조)

## 실행
1. 해당 폴더에서필요한 패키지 라이브러리를 설치
````sh
$ npm i
````
2. 프로그램을 실행하여 엑셀 파일을 생성
````sh
-- app_col 테스트(test_row.xlsx 파일 생성)
$ node app_col.js

-- app_row 테스트 (test_row.xlsx 파일 생성)
$ node app_row.js
````
## 검증
- test_col.xls 와 test_row.xlsx 파일을 읽어서 검증

## 개선
- 데이터 검증을 위해서는 일단 RDB에 데이터를 저장 후 엑셀 생성